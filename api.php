<?php
require_once('config.php');
include_once('functions.php');

if(!isset($_GET['action']) || strlen($_GET['action']) == 0)
    exit();

//if(!isset($_GET['codeStation']) || intval($_GET['codeStation']) == 0)
//    exit();

header('Content-Type: application/json');

//$codeStation = intval($_GET['codeStation']);

switch($_GET['action'])
{
    case 'getDataStation':
    echo json_encode(getDataStation());
    exit();
    break;
    case 'getDataVelib':
    echo json_encode(getDataVelib());
    exit();
    break;
}

function getDataVelib($laps = "1year")
{
    global $pdo;

    //Filtre 1 heure
    $hier = new DateTime("-".$laps);
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $query = 'SELECT nbBike,nbEbike,nbFreeEDock,date FROM statusConso';
    $query .= ' WHERE date >= "'.$filtreDate.'"';
    $query .= ' ORDER BY date ASC';

    $requete = $pdo->query($query);
    $allStation = $requete->fetchAll();
    $dates = [];
    $nbVelibData = [];
    $nbEVelibData = [];
    $nbFreeDockData = [];
    foreach($allStation as $i => $c)
    {
        $nbVelibData[] = array(x => (new DateTime($c['date']))->format('D M d Y H:i:s O'), y => intval($c['nbBike']));
        $nbEVelibData[] = array(x => (new DateTime($c['date']))->format('D M d Y H:i:s O'), y => intval($c['nbEbike']));
        $nbFreeDockData[] = array(x => (new DateTime($c['date']))->format('D M d Y H:i:s O'), y => intval($c['nbFreeEDock']));
    }

    return array(
        array(
            'name' => 'Vélos mécaniques',
            'type' => 'stackedArea',
            'showInLegend' => 'True',
            'dataPoints' => $nbVelibData
        ),
        array(
            'name' => 'Vélos éléctriques',
            'type' => 'stackedArea',
            'showInLegend' => 'True',
            'dataPoints' => $nbEVelibData
        ),
        array(
            'name' => 'Places libres',
            'type' => 'stackedArea',
            'showInLegend' => 'True',
            'dataPoints' => $nbFreeDockData
        ),
    );
}

function getDataStation($laps = "1year")
{
    global $pdo;

    //Filtre 1 heure
    $hier = new DateTime("-".$laps);
    $filtreDate = $hier->format('Y-m-d H:i:s');

    # Get only timestamp where the number of stations changed
    $query  = ' SELECT nbStation, date';
    $query .= ' FROM';
    $query .= '   ( SELECT (@nbStationPre <> nbStation) AS stationChanged';
    $query .= '          , nbStation, date';
    $query .= '          , @nbStationPre := nbStation';
    $query .= '     FROM statusConso';
    $query .= '          , (SELECT @nbStationPre:=NULL) AS d';
    $query .= '     ORDER BY date';
    $query .= '   ) AS good';
    $query .= ' WHERE stationChanged ;';

    $requete = $pdo->query($query);
    $allStation = $requete->fetchAll();
    $dates = [];
    $nbStationsData = [];
    foreach($allStation as $i => $c)
    {
        if($c['nbStation'] > 0)
        {
            $nbStationsData[] = array(x => (new DateTime($c['date']))->format('D M d Y H:i:s O'), y => intval($c['nbStation']));
        }
    }
    //print_r($nbStationsData);

    return array(array(
        'name' => 'Nombre de stations',
        'type' => 'area',
        'showInLegend' => 'True',
        'dataPoints' => $nbStationsData
    ));
}

?>
