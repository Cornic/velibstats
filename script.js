function getData(url)
{
    return new Promise(
        function(resolve, reject)
        {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onload = function()
            {
                if (xhr.status === 200)
                    resolve(JSON.parse(xhr.response));
                else
                    reject(xhr.statusText);
            };

            xhr.send();
        }
    );
}

var graphInstance = {};
var graphData = null;
var graphDataSets = null;
function displayGraph(actionUrl, chartID)
{
    getData('api.php?action='+actionUrl+'&codeStation='+codeStation).then(function(data)
    {
        var graphdata;

        // prepare the data with correct date
        data.forEach(function(data) {
            data["dataPoints"].forEach(function(el) {
                el.x = new Date(el.x);
            });
        });

        // prepare each graph
        if (chartID === "chartNbStations")
        {
            graphdata = {
                theme: 'light2',
                animationEnabled: false,
                zoomEnabled: true,
                title: {text: "Nombre de stations (zoom possible)"},
                axisX: {interval: 1, intervalType: "day"},
                toolTip:{
                    contentFormatter: function ( e ) {
                        return e.entries[0].dataPoint.x.toLocaleString("fr-FR") + " : " +  e.entries[0].dataPoint.y;
                    }
                },
                data: data
            };
        }
        else
        {
            graphdata = {
                theme: 'light2',
                animationEnabled: false,
                zoomEnabled: true,
                title: {text: "Nombre de vélos (zoom possible)"},
                axisX: {interval: 1, intervalType: "day"},
                toolTip:{
                    shared: true
                },
                data: data
            };
        }

        CanvasGraph = new CanvasJS.Chart(chartID, graphdata);
        CanvasGraph.render();
    });
}

$(document).ready( function () {
    displayGraph("getDataStation", "chartNbStations");
    displayGraph("getDataVelib", "chartNbVelib");
});

var dtTraduction = {
    "sProcessing":     "Traitement en cours...",
    "sSearch":         "Rechercher&nbsp;:",
    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
    "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    "sInfoPostFix":    "",
    "sLoadingRecords": "Chargement en cours...",
    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
    "oPaginate": {
        "sFirst":      "Premier",
        "sPrevious":   "Pr&eacute;c&eacute;dent",
        "sNext":       "Suivant",
        "sLast":       "Dernier"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
    }
};

